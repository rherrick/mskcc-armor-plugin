/*
 * mskcc-armor-plugin: org.mskcc.conrad.plugins.armor.dicom.ArmorDicomObjectIdentifier
 */

package org.mskcc.conrad.plugins.armor.dicom;

import lombok.extern.slf4j.Slf4j;
import org.dcm4che2.data.Tag;
import org.nrg.dcm.ChainExtractor;
import org.nrg.dcm.ContainedAssignmentExtractor;
import org.nrg.dcm.TextExtractor;
import org.nrg.dcm.id.CompositeDicomObjectIdentifier;
import org.nrg.dcm.id.FixedDicomProjectIdentifier;
import org.nrg.xdat.security.user.XnatUserProvider;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
@Slf4j
public class ArmorDicomObjectIdentifier extends CompositeDicomObjectIdentifier {
    public ArmorDicomObjectIdentifier(final String name, final String projectId, final XnatUserProvider userProvider) {
        super(name, new FixedDicomProjectIdentifier(projectId), // This always routes to the specified project
              new TextExtractor(Tag.AccessionNumber),           // Get the subject ID from the accession number
              new TextExtractor(Tag.PatientName),               // Get the session label from the patient name
              new ChainExtractor(new ContainedAssignmentExtractor(Tag.PatientComments, "AA", Pattern.CASE_INSENSITIVE), new ContainedAssignmentExtractor(Tag.StudyComments, "AA", Pattern.CASE_INSENSITIVE)));
        setUserProvider(userProvider);
        log.info("Created new identifier instance for project {} with user {}", projectId, userProvider.getLogin());
    }
}
