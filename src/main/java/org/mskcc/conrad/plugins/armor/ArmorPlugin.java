/*
 * mskcc-armor-plugin: org.mskcc.conrad.plugins.armor.ArmorPlugin
 */

package org.mskcc.conrad.plugins.armor;

import lombok.extern.slf4j.Slf4j;
import org.mskcc.conrad.plugins.armor.dicom.ArmorDicomObjectIdentifier;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.user.XnatUserProvider;
import org.nrg.xnat.DicomObjectIdentifier;
import org.springframework.context.annotation.Bean;

@XnatPlugin(value = "mskcc-armor-plugin", name = "MSKCC Armor DICOM Object Identifier Plugin", description = "Defines the armorDicomObjectIdentifier bean that can be used for routing incoming DICOM data to the appropriate project and subject.")
@Slf4j
public class ArmorPlugin {
    @Bean
    public DicomObjectIdentifier<XnatProjectdata> armorDicomObjectIdentifier(final XnatUserProvider receivedFileUserProvider) {
        return new ArmorDicomObjectIdentifier("ARMO", "ARMO", receivedFileUserProvider);
    }
}
